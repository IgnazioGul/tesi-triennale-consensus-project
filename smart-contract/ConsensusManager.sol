// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract ConsensusManager {
    
    address public owner;
    
    event ConsentChange(uint indexed UUID, uint indexed study_id, bool use_consent, bool forwarding_consent, uint256 timestamp ); //maybe uint64? Does it cost?
    
    constructor() payable  
    { owner = msg.sender; }

    //modifier to check if user different than owner tried to execute a particular function
    modifier isOwner(){
        require(msg.sender == owner, "Non sei autorizzato.");
        _;
    }
    
    //fallback to get eth value
    function deposit() external payable{
        
    }
    
    //fucntion to widthdraw from SC balance
	function withdraw(uint withdraw_amount) public {
		require(withdraw_amount <= 0.1 ether);
		require(address(this).balance >= withdraw_amount, "Insufficient balance");
		payable(msg.sender).transfer(withdraw_amount);
	}
    
    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }
    
    //function to write into logs current consent change 
    function changeConsent(uint UUID, uint study_id, bool use_consent, bool forwarding_consent) public isOwner {
        emit ConsentChange(UUID, study_id, use_consent, forwarding_consent, block.timestamp);
    }
    
    //function to destroy contract
    function destroy() public isOwner{
        selfdestruct(payable(owner));
    }
    
    
}