package dmi.tesi.service;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.BaseEventResponse;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 1.4.1.
 */
@SuppressWarnings("rawtypes")
public class ConsensusManager extends Contract {
    public static final String BINARY = "6080604052600080546001600160a01b03191633179055610349806100256000396000f3fe6080604052600436106100555760003560e01c806312065fe01461005a5780632e1a7d4d1461007a57806375e17e961461009c57806383197ef0146100bc5780638da5cb5b146100d1578063d0e30db01461009a575b600080fd5b34801561006657600080fd5b506040514781526020015b60405180910390f35b34801561008657600080fd5b5061009a6100953660046102b6565b610109565b005b3480156100a857600080fd5b5061009a6100b73660046102ce565b61019b565b3480156100c857600080fd5b5061009a610242565b3480156100dd57600080fd5b506000546100f1906001600160a01b031681565b6040516001600160a01b039091168152602001610071565b67016345785d8a000081111561011e57600080fd5b8047101561016a5760405162461bcd60e51b8152602060048201526014602482015273496e73756666696369656e742062616c616e636560601b60448201526064015b60405180910390fd5b604051339082156108fc029083906000818181858888f19350505050158015610197573d6000803e3d6000fd5b5050565b6000546001600160a01b031633146101ec5760405162461bcd60e51b81526020600482015260146024820152732737b71039b2b49030baba37b934bd3d30ba379760611b6044820152606401610161565b82847febb90ccf15161cd0df11b309eddc759bb6582ef4d84a0fa02dae4213f4fe8a578484426040516102349392919092151583529015156020830152604082015260600190565b60405180910390a350505050565b6000546001600160a01b031633146102935760405162461bcd60e51b81526020600482015260146024820152732737b71039b2b49030baba37b934bd3d30ba379760611b6044820152606401610161565b6000546001600160a01b0316ff5b803580151581146102b157600080fd5b919050565b6000602082840312156102c7578081fd5b5035919050565b600080600080608085870312156102e3578283fd5b84359350602085013592506102fa604086016102a1565b9150610308606086016102a1565b90509295919450925056fea264697066735822122047b5a1d22979ce3c66047d7cc457775dbed4f5286275566954270e9af93643cd64736f6c63430008040033";

    public static final String FUNC_CHANGECONSENT = "changeConsent";

    public static final String FUNC_DEPOSIT = "deposit";

    public static final String FUNC_DESTROY = "destroy";

    public static final String FUNC_GETBALANCE = "getBalance";

    public static final String FUNC_OWNER = "owner";

    public static final String FUNC_WITHDRAW = "withdraw";

    public static final Event CONSENTCHANGE_EVENT = new Event("ConsentChange", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>(true) {}, new TypeReference<Uint256>(true) {}, new TypeReference<Bool>() {}, new TypeReference<Bool>() {}, new TypeReference<Uint256>() {}));
    ;

    @Deprecated
    protected ConsensusManager(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected ConsensusManager(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected ConsensusManager(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected ConsensusManager(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public List<ConsentChangeEventResponse> getConsentChangeEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(CONSENTCHANGE_EVENT, transactionReceipt);
        ArrayList<ConsentChangeEventResponse> responses = new ArrayList<ConsentChangeEventResponse>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            ConsentChangeEventResponse typedResponse = new ConsentChangeEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.UUID = (BigInteger) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.study_id = (BigInteger) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.use_consent = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.forwarding_consent = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<ConsentChangeEventResponse> consentChangeEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new Function<Log, ConsentChangeEventResponse>() {
            @Override
            public ConsentChangeEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(CONSENTCHANGE_EVENT, log);
                ConsentChangeEventResponse typedResponse = new ConsentChangeEventResponse();
                typedResponse.log = log;
                typedResponse.UUID = (BigInteger) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.study_id = (BigInteger) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.use_consent = (Boolean) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.forwarding_consent = (Boolean) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.timestamp = (BigInteger) eventValues.getNonIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<ConsentChangeEventResponse> consentChangeEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(CONSENTCHANGE_EVENT));
        return consentChangeEventFlowable(filter);
    }

    public RemoteFunctionCall<TransactionReceipt> changeConsent(BigInteger UUID, BigInteger study_id, Boolean use_consent, Boolean forwarding_consent) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_CHANGECONSENT, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(UUID), 
                new org.web3j.abi.datatypes.generated.Uint256(study_id), 
                new org.web3j.abi.datatypes.Bool(use_consent), 
                new org.web3j.abi.datatypes.Bool(forwarding_consent)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> deposit() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_DEPOSIT, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction (function);
    }

    public RemoteFunctionCall<TransactionReceipt> destroy() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_DESTROY, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<BigInteger> getBalance() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_GETBALANCE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteFunctionCall<String> owner() {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(FUNC_OWNER, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteFunctionCall<TransactionReceipt> withdraw(BigInteger withdraw_amount) {
        final org.web3j.abi.datatypes.Function function = new org.web3j.abi.datatypes.Function(
                FUNC_WITHDRAW, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(withdraw_amount)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    @Deprecated
    public static ConsensusManager load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new ConsensusManager(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static ConsensusManager load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new ConsensusManager(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static ConsensusManager load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new ConsensusManager(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static ConsensusManager load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new ConsensusManager(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<ConsensusManager> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(ConsensusManager.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    public static RemoteCall<ConsensusManager> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(ConsensusManager.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<ConsensusManager> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(ConsensusManager.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<ConsensusManager> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(ConsensusManager.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static class ConsentChangeEventResponse extends BaseEventResponse {
        public BigInteger UUID;

        public BigInteger study_id;

        public Boolean use_consent;

        public Boolean forwarding_consent;

        public BigInteger timestamp;
    }
}
