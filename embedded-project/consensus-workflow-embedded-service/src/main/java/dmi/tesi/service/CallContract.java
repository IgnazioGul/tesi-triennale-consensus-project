package dmi.tesi.service;

import java.math.BigInteger;

import org.drools.core.process.instance.WorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

@Component("CallContract")
public class CallContract implements WorkItemHandler {

    private static final  String CONTRACT_ADDRESS = "0x8c1EAC44b529939Aa2F77eaB6FCb1AE664C1F3A5";
    private static final  String pvKey = "f3145434b3847676f25e3252f7f2dc9fe3c4bc04c01acf1f7bfb1600d47c8b08";

    @Override
    public void executeWorkItem(WorkItem workItem, org.kie.api.runtime.process.WorkItemManager manager) {

        //lambda di new Runnable() e void run()
        new Thread( () -> {

                // input from context
                Integer UUID = (Integer) workItem.getParameter("UUID");
                Integer study_id = (Integer) workItem.getParameter("study_id");
                Boolean use_consent = (Boolean) workItem.getParameter("use_consent");
                Boolean forwarding_consent = (Boolean) workItem.getParameter("forwarding_consent");

                // connect to node to interact (okHttp3 has conflict with spring okhttp3)
                Web3j myEtherWallet = Web3j
                        .build(new HttpService("https://ropsten.infura.io/v3/c85d7c4cbc37452b8cec8786c1180787"));

                // login info (pvkey, pbkey) throws error
                Credentials credentials = Credentials.create(pvKey);

                // NB: these are in Wei unit
                final BigInteger gasPrice = BigInteger.valueOf(2000000000); // 19.98 Gwei
                final BigInteger gasLimit = BigInteger.valueOf(61000);

                // import contract wrapper
                final ConsensusManager contract = ConsensusManager.load(CONTRACT_ADDRESS, myEtherWallet, credentials,
                        gasPrice, gasLimit);

                try {
                     contract.changeConsent(BigInteger.valueOf(UUID),
                    BigInteger.valueOf(study_id), use_consent, forwarding_consent).send();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            
        }).start();
        //NB: IF COMPLETED INSIDE THREAD RUN, THROWS NULLPOINTER EXCEPTION
        manager.completeWorkItem(workItem.getId(), null);

    }

    @Override
    public void abortWorkItem(WorkItem workItem, org.kie.api.runtime.process.WorkItemManager manager) {
        // TODO Auto-generated method stub

    }

}
