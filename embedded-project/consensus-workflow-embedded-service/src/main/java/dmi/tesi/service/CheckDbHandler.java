package dmi.tesi.service;

import org.kie.api.runtime.process.WorkItem;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.springframework.stereotype.Component;

@Component("CheckDb")
public class CheckDbHandler implements WorkItemHandler {

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {

				// prendo gli input collegati alle process vars
				Integer partner_id = (Integer) workItem.getParameter("partner_id");
				Integer study_id = (Integer) workItem.getParameter("study_id");
				if (study_id == -1)
					manager.completeWorkItem(workItem.getId(), null); // completo il workitem, meglio se sistemi il frontend per scelta dello studio

				// connessione al db e query di verifica. La query restituirà una entry o
				// nessuna.
				String url = "jdbc:mysql://localhost:3306/consensus_project";
				String user = "manager";
				String password = "manager";

				String query = "SELECT * FROM `partner_id`" + " WHERE `user_id`=" + partner_id + " AND `study_id`="
						+ study_id;

				Connection con;
				Statement st;
				ResultSet rs;
				Map<String, Object> result = new HashMap<>();
				try {
					con = DriverManager.getConnection(url, user, password);
					st = con.createStatement();
					rs = st.executeQuery(query);
					if (rs.next()) { // esiste la entry
						// ottieni UUID, assegnalo all'output
						int UUID = rs.getInt(1);
						result.put("UUID", UUID);
					} else { // non esiste una entry per la coppia
								// quindi viene creata e il nuovo UUID viene mandato in output
						java.util.Date date = new java.util.Date();
						java.sql.Timestamp ts = new java.sql.Timestamp(date.getTime()); // timestamp per l'insert
						String insert = "INSERT INTO partner_id "
								+ "(`user_id`, `study_id`, `created_at`, `updated_at`) " + "VALUES (" + partner_id
								+ ", " + study_id + ", '" + ts + "', '" + ts + "');";
						PreparedStatement ps = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS); // ritorna l'id generato (nuovo UUID)
						ps.execute();

						ResultSet res = ps.getGeneratedKeys();
						int UUID = 0;
						if (res.next()) {
							UUID = res.getInt(1);
						}
						result.put("UUID", UUID); // mando in output l'UUID
					}
					con.close(); // chiudo la connessione al db
					manager.completeWorkItem(workItem.getId(), result); // completo il workitem con l'output

				} catch (SQLException e) {
					e.printStackTrace();
				}

	}

	
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		// TODO Auto-generated method stub
	}

}
