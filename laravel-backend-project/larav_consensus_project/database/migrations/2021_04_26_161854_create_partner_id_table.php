<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePartnerIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_id', function (Blueprint $table) {
            $table->id(); //UUID, dovrebbe partire tipo da 14000
            $table->foreignId('user_id')->references('id')->on('users'); //FK allo pseudonimo
            $table->foreignId('study_id')->references('id')->on('studies'); //FK allo pseudonimo
            $table->timestamps();
        });
        DB::statement("ALTER TABLE partner_id AUTO_INCREMENT = 14000;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_id');
    }
}
