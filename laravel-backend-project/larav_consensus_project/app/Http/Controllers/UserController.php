<?php

namespace App\Http\Controllers;

use App\Models\Study;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getHome()
    {
        $studies = Study::all();
        return view('forms.send-consent')
            ->with(compact('studies'));
    }
}
