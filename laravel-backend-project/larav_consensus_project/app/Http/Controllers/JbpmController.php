<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class JbpmController extends Controller
{
    protected $CONTAINER_ID = 'consensus-workflow-embedded-kjar-1_0-SNAPSHOT';
    protected $PROCESS_ID = 'consensus-workflow-embedded-kjar.data-workflow';
    protected $BASE_PATH = 'http://localhost:8090/rest';
    protected $client;

    /**
     * JbpmController constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }


    public function submitConsent(Request $request)
    {
        $userId = (int) $request->get('userId');
        $studyId = (int) $request->get('studyId');
        //checkbox, se impostata has ritorna 1, altrimenti null
        $use_cons = $request->has('use_consent');
        $frw_cons = $request->has('forwarding_consent');

        //problema, start process non ritorna!! Ma ottiene la risposta?
        try {
            $this->startProcess($userId, $studyId, $use_cons, $frw_cons);
        } catch (GuzzleException $e) {
        }

        return redirect()->back();
    }

    /**
     * Avvia una process instance del data-workflow. Passa i datti delle var di contesto. Ritorna l'id dell'instanza creata
     * @param $partnerId
     * @param $studyId
     * @param $use_consent
     * @param $forwarding_consent
     * @return mixed
     * @throws GuzzleException
     */
    public function startProcess($partnerId, $studyId, $use_consent, $forwarding_consent)
    {

        $url = $this->BASE_PATH . '/server/containers/' . $this->CONTAINER_ID . '/processes/' . $this->PROCESS_ID . '/instances';
        $response = $this->client->request('POST', $url,
            [
                'auth' => ['user', 'user'],
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'body' => json_encode(['partner_id' => $partnerId,
                    'study_id' => $studyId,
                    'use_consent' => $use_consent,
                    'forwarding_consent' => $forwarding_consent
                ])
            ]);
        //il response arriva solo dopo il completamento del task. Perchè?
        return json_decode($response->getBody());
    }

    /** Ritorna l'id del task umano setText, per passare i dati del consenso
     * @param $processId
     * @return mixed
     * @throws GuzzleException
     */
    //PARAMETRO TASKNAME E CONTROLLO
    //HUMAN TASK VALORI DI OUTPUT
    public function getHumanTaskId($processId)
    {
        $url = $this->BASE_PATH . '/server/containers/' . $this->CONTAINER_ID . '/processes/instances/' . $processId;
        $json = $this->client->request('GET', $url,
            [
                'auth' => ['user', 'user'],
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']

            ]);
        //parsing json reponse to get task ID
        //$taskIstanceId = $response->{'active-user-tasks'}->{'task-summary'}[0]->{'task-id'}

        $response = json_decode($json->getBody());
        return $response->{'active-user-tasks'}->{'task-summary'}[0]->{'task-id'}; //returning task id
    }

    /** Avvia un task umano. Non ritorna nulla
     * @param $taskInstanceId
     * @throws GuzzleException
     */
    //START HUMAN TASK
    public function startTask($taskInstanceId)
    {
        $url = $this->BASE_PATH . '/server/containers/' . $this->CONTAINER_ID . '/tasks/' . $taskInstanceId . '/states/started';
        $this->client->request('PUT', $url,
            [
                'auth' => ['user', 'user'],
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json']
            ]);
    }

    /** Completa il task passando i dati provenienti dal frontend
     * @param $taskInstanceId
     * @param $partnerId
     * @param $studyId
     * @param $use_consent
     * @param $forwarding_consent
     * @throws GuzzleException
     */
    public function completeTask($taskInstanceId, $partnerId, $studyId, $use_consent, $forwarding_consent)
    {
        $url = $this->BASE_PATH . '/server/containers/' . $this->CONTAINER_ID . '/tasks/' . $taskInstanceId . '/states/completed';
        $this->client->request('PUT', $url,
            [
                'auth' => ['user', 'user'],
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'body' => json_encode(['partner_id' => $partnerId,
                    'study_id' => $studyId,
                    'use_consent' => $use_consent,
                    'forwarding_consent' => $forwarding_consent
                ])
            ]);
    }
}
