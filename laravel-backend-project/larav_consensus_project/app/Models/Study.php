<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    use HasFactory;

    protected $table = 'studies';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partnersIds()
    {
        return $this->hasMany(PartnerId::class);
    }
}
