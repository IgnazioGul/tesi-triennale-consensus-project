<?php

use App\Http\Controllers\JbpmController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', [UserController::class, 'getHome'])
    ->middleware(['auth'])
        ->name('dashboard');

Route::post('/dashboard', [JbpmController::class, 'submitConsent'])
    ->middleware(['auth'])
        ->name('consent.submit');

require __DIR__.'/auth.php';
