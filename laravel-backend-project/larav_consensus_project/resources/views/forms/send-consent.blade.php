@extends('template')

@section('styles')

@endsection

@section('content')
    <div class="content">
        <div class="note">
            <p>Manage your consent </p>
        </div>

    <form method="post" action="{{route('consent.submit')}}">
        <div class="form-content">
            <div class="row mb-5 mt-2">
                <div class="col-md-6">
                    @csrf
                    <select name="studyId" class="custom-select">
                        @if(isset($studies))
                            <option selected value="-1">Choose the research project.</option>
                            @foreach($studies as $study)
                                <option value="{{$study->id}}">{{$study->name}}</option>
                            @endforeach
                        @else
                            <option selected>No studies available.</option>
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <div class="form-check form-check-inline mt-md-3">
                        <input class="form-check-input" type="checkbox" name="use_consent" id="inlineRadio1" value="1">
                        <label class="form-check-label mr-3" for="inlineRadio1">Usage consensus.</label>
                        <input class="form-check-input " type="checkbox" name="forwarding_consent" id="inlineRadio2" value="1">
                        <label class="form-check-label" for="inlineRadio1">Forwarding consensus.</label>

                        <input name="userId" type="hidden" value="1">
                    </div>
                </div>
            </div>
            <button type="submit" class="btnSubmit">Submit</button>
        </div>
    </form>
    </div>
@endsection

@section('scripts')
@endsection
