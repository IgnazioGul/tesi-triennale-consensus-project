
# Consensus managment project
Il progetto sulla gestione del consenso dinamico nasce con lo scopo di fornire flessibilità e trasparenza nella gestione del consenso sottoscritta dagli interessati.

L'aspetto implementativo viene gestito attraverso un workflow [JBPM](https://jbpm.org/), garantendo una esecuzione compartimentalizzata dei task, riusabile e del tutto slegata dal front-end. 

Per ragioni legate alla gestione dei dati, quali immutabilità e trasparenza, la raccolta del consenso avviene attraverso delle scritture su blockchain, precisamente come log di una transazione su Ethereum.

Infine è presente una web-app in [Laravel 8](https://laravel.com/) che permette la sottoscrizione del consenso, dialogando quindi con l'engine JBPM e fornendo una interfaccia grafica all'interessato.

## Core files


  
* Java task handlers : `.\embedded-project\consensus-workflow-embedded-service\src\main\java\dmi\tesi\service`
* Smart contract source .sol : `.\smart-contract\ConsensusManager.sol`
* Laravel 8 app :  `.\laravel-backend-project\larav_consensus_project`


## Utilizzo
Basic 

```bash
git clone https://IgnazioGul@bitbucket.org/IgnazioGul/tesi-triennale-consensus-project.git
```

Avvio dell'engine JBPM

```bash
cd .\embedded-project\consensus-workflow-embedded-service

.\launch.bat clean install
```

Avvio dell'applicazione Laravel raggiungibile all'indirizzo [http://localhost:8000](http://localhost:8000)

```bash
cd .\laravel-backend-project\larav_consensus_project

php artisan serve
```


